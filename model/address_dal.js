var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(address_id, callback) {
    var query = 'SELECT * FROM address WHERE address_id = ?';
    var queryData = [address_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.delete = function(address_id, callback) {
    var query = 'DELETE FROM company_address WHERE address_id = ?';
    var queryData = [address_id];

    connection.query(query,queryData);

    var query = 'DELETE FROM address WHERE address_id = ?';

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.insert = function(params, callback) {

    // INSERT THE STREET AND ZIP_CODE
    var query = 'INSERT INTO address (street,zip_code) VALUES (?)';

    var queryData = [params.street, params.zip_code];

    connection.query(query,  function(err, result) {

/*        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var company_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var companyAddressData = [];
        if (params.address_id.constructor === Array) {
            for (var i = 0; i < params.address_id.length; i++) {
                companyAddressData.push([company_id, params.address_id[i]]);
            }
        }
        else {
            companyAddressData.push([company_id, params.address_id]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [companyAddressData], function(err, result){
            callback(err, result);
        });*/
    });

};

exports.edit = function(address_id, callback) {
    var query = 'CALL address_getinfo(?)';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE address set zip_code = ? WHERE address_id = ?';
    var queryData = [params.address_id, params.zip_code, params.address_id];
    connection.query(query, queryData);

    var query = 'UPDATE address SET street = ? WHERE address_id = ?';
    var queryData = [params.street, params.address_id];

    connection.query(query, queryData, function(err, result) {
            callback(err,result);
        //delete company_address entries for this company
        /*companyAddressDeleteAll(params.company_id, function(err, result){

            if(params.address_id != null) {
                //insert company_address ids
                companyAddressInsert(params.company_id, params.address_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });*/

    });
};