var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM skill;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(skill_id, callback) {
    var query = 'SELECT * FROM skill WHERE skill_id = ?';
    var queryData = [skill_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.delete = function(skill_id, callback) {
    var query = 'DELETE FROM skill WHERE skill_id = ?';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.insert = function(params, callback) {

    // INSERT THE name and description
    var query = 'INSERT INTO skill (skill_name, description) VALUES (?)';

    var queryData = [params.skillNameAndDescription];

    connection.query(query,  function(err, result) {

        /*        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
                var company_id = result.insertId;

                // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
                var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

                // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
                var companyAddressData = [];
                if (params.address_id.constructor === Array) {
                    for (var i = 0; i < params.address_id.length; i++) {
                        companyAddressData.push([company_id, params.address_id[i]]);
                    }
                }
                else {
                    companyAddressData.push([company_id, params.address_id]);
                }

                // NOTE THE EXTRA [] AROUND companyAddressData
                connection.query(query, [companyAddressData], function(err, result){
                    callback(err, result);
                });*/
    });

};