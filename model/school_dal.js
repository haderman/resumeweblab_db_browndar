var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM school;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(school_id, callback) {
    var query = 'SELECT * FROM school WHERE school_id = ?';
    var queryData = [school_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.delete = function(school_id, callback) {
    var query = 'DELETE FROM school WHERE school_id = ?';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.insert = function(params, callback) {

    // FIRST INSERT THE school
    var query = 'INSERT INTO school (school_name) VALUES (?)';

    var queryData = [params.school_name];

    connection.query(query, params.school_name, function(err, result) {

        // THEN USE THE school_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO school
        var school_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'UPDATE school SET address_id = VALUES ? WHERE school_name = params.school_name';

    });

};

/*exports.insert = function(params, callback) {

    // INSERT THE STREET AND ZIP_CODE
    var query = 'INSERT INTO school (school_name, address_id) VALUES (?)';

    var queryData = [params.nameAndAddress];

    connection.query(query,  function(err, result) {

        /*        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
                var company_id = result.insertId;

                // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
                var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

                // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
                var companyAddressData = [];
                if (params.address_id.constructor === Array) {
                    for (var i = 0; i < params.address_id.length; i++) {
                        companyAddressData.push([company_id, params.address_id[i]]);
                    }
                }
                else {
                    companyAddressData.push([company_id, params.address_id]);
                }

                // NOTE THE EXTRA [] AROUND companyAddressData
                connection.query(query, [companyAddressData], function(err, result){
                    callback(err, result);
                });
    });


};
*/


exports.update = function(params, callback) {
    var query = 'UPDATE school SET school_name = ? WHERE school_id = ?';
    var queryData = [params.school_name, params.school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err,result);
    });
};

exports.edit = function(school_id, callback) {
    var query = 'SELECT * from school WHERE school_id = ?';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};